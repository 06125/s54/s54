let collection = [];

// Write the queue functions below.
const print = () => collection;

const enqueue = (element) => {
  // collection.push(element);

  collection[collection.length] = element;
  return collection;
};

const dequeue = () => {
  // collection.shift();

  collection.splice(0, 1);
  return collection;
};

const front = () => collection[0];
const size = () => collection.length;
const isEmpty = () => (collection.length ? false : true);

module.exports = {
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty,
};
